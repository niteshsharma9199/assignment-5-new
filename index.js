// api url 
const api_url =  
      "https://restcountries.eu/rest/v2/all"; 

let search = document.getElementById('search');
let search_term;
let data;
let cinfo;
  
// Defining async function 
async function getapi(url) { 
    
    // Storing response 
    const response = await fetch(url); 
    
    // Storing data in form of JSON 
    data = await response.json(); 
    if (response) { 
        console.log('getapi query passed')
    } 
    show(data); 
} 
// async function getInfo(url) { 
    
//     // Storing response 
//     const response = await fetch(url); 
    
//     // Storing data in form of JSON 
//     cinfo = await response.json(); 
//     if (response) { 
//         console.log('GetInfo query passed')
//     } 
//     // console.log(cinfo);
//     showDetails(cinfo);
// } 
// Calling that async function 
getapi(api_url); 
  
// Function to define innerHTML for HTML table 
function show(data) { 
    let tab = '';
    
    // Loop to access all rows  
    for (let r of data) { 
        tab +=  
        `
        <div class="card mb-3">
            <div class="row g-0">
              <div class="col-md-6">
                <img src="${r.flag}" alt="..." class="img-fluid" >
              </div>
              <div class="col-md-6">
                <div class="card-body">
                  <h5 class="card-title">${r.name}</h5>
                  <p class="card-text">Currencies: ${r.currencies[0].name}</p>
                  <p class="card-text">Timezones: ${r.timezones}</p>
                   
                    <!-- buttons -->
                    <div class="row">
                    <div class="col-md-6 mt-2">
                    <a href="https://www.google.co.in/maps/place/${data[0].name}/" class="btn btn-primary">Show Map</a>
                    </div>
                    <div class="col-md-6 mt-2">
                   
                     <a href="details.html">
                     <button onclick="detailinfo(this.value)" value="${r.name}" id="buttonId"  class="btn btn-primary">Details</button>
                     </a>
                    
                    </div>


                    </div>


                  
                </div>
              </div>
            </div>
          </div>

        `;

    } 
    // Setting innerHTML as tab variable 
    document.getElementById("countries").innerHTML = tab; 
} 


search.addEventListener('input', (e) => {
  search_term = e.target.value;

  showResults(search_term.toUpperCase());
});

function showResults(search_term) {
  let countries = document.getElementById('countries');
  // countries.classList.add('invisible');
  resultData = data.filter((country) => {

    return country.name.toUpperCase().startsWith(search_term);
  });

  console.log(resultData);
    
    // Loop to access all rows  
  show(resultData);
}

function detailinfo(val){
  let tab=val;
  console.log(tab)
  window.localStorage.setItem('country', val);
  // countries.classList.add('invisible');
  // const api = `https://restcountries.eu/rest/v2/name/${tab}?fullText=true`
  // getInfo(api);
}

// function showDetails(data) {
//   let detail = document.getElementById('details');
//   detail.innerHTML = `
        
//           <div class="card mb-3">
//             <div class="row g-0">
//               <div class="col-md-6">
//                 <img src="${data[0].flag}" alt="..." class="img-fluid max-width: 100%; height: auto;" >
//               </div>
//               <div class="col-md-6">
//                 <div class="card-body">
//                   <h5 class="card-title">${data[0].name}</h5>
//                   <p class="card-text">Currencies: ${data[0].currencies[0].name}</p>
//                   <p class="card-text">Capital: ${data[0].capital}</p>
//                   <p class="card-text">Population: ${data[0].population}</p>
//                   <p class="card-text">Region: ${data[0].region}</p>
//                   <p class="card-text">Sub-region: ${data[0].subregion}</p>
//                   <p class="card-text">Area: ${data[0].area}</p>
//                   <p class="card-text">Country Code: ${data[0].callingcodes}</p>
//                   <p class="card-text">Timezones: ${data[0].timezones}</p>
//                   <!-- buttons -->
//                   <div class="row">
//                       <div class="col=sm=12">
//                           <a href="https://www.google.co.in/maps/place/${data[0].name}/" class="btn btn-primary" >Show Map</a>
//                       </div>
                      
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>

//         `;
// }

