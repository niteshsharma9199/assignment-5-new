let selectedCountry=window.localStorage.getItem('country');
console.log(selectedCountry);

async function getInfo(url) { 
    
    // Storing response 
    const response = await fetch(url); 
    
    // Storing data in form of JSON 
    cinfo = await response.json(); 
    if (response) { 
        console.log('GetInfo query passed')
    } 
    // console.log(cinfo);
    showDetails(cinfo);
} 

const api = `https://restcountries.eu/rest/v2/name/${selectedCountry}?fullText=true`
  getInfo(api);


function showDetails(data) {
  let detail = document.getElementById('details');
  detail.innerHTML = `
          <div><h2>Country</h2></div>
          <div class="row border">
              <div class="col-md-6">
                <div class="row"><img src="${data[0].flag}" alt="..." class="img-fluid max-width: 100%; height: auto;" ></div>
                
              </div>

              <div class="col-md-6">
                <div class="row">
                  <div class="col-sm-12"> <h5>${data[0].name}</h5> </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Country:</h5> </div>
                  <div class="col-sm-6"> ${data[0].name} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Currency:</h5> </div>
                  <div class="col-sm-6"> ${data[0].currencies[0].name} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Capital:</h5> </div>
                  <div class="col-sm-6"> ${data[0].capital} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Population:</h5> </div>
                  <div class="col-sm-6"> ${data[0].population} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Region:</h5> </div>
                  <div class="col-sm-6"> ${data[0].region} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Sub-region:</h5> </div>
                  <div class="col-sm-6"> ${data[0].subregion} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Area:</h5> </div>
                  <div class="col-sm-6"> ${data[0].area} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Country Code:</h5> </div>
                  <div class="col-sm-6"> ${data[0].alpha3Code} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Timezones:</h5> </div>
                  <div class="col-sm-6"> ${data[0].timezones[0]} </div>
              </div>

          </div>











          
        `;
}